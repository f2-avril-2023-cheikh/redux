import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  products: [],
  product: {}
}

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    add: (state, action) => {
      state.products.push(action.payload)
    },
    find: (state, action) => {
      state.product = state.products.find(value => value.id == action.payload)
    },
    remove: (state, action) => {
      state.products = state.products.filter(value => value.id != action.payload)
    },
    update: (state, action) => {
      state.products.map(product =>{
        if(product.id == action.payload.id){
          product.name = action.payload.name
          product.description = action.payload.description
          product.quantity = action.payload.quantity
          product.price = action.payload.price
          return product
        }
        return product
      })
    },
  },
})

// Action creators are generated for each case reducer function
export const { add, find, remove, update } = productSlice.actions

export default productSlice.reducer