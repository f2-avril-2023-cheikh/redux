import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/navbars/Header";
import Home from "./components/Home";
import NoPage from "./components/NoPage";
import AddProduct from "./components/products/AddProduct";
import AllProduct from "./components/products/AllProduct";
import ProductItem from "./components/products/ProductItem";
import UpdateProduct from "./components/products/UpdateProduct";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Header />}>
          <Route index element={<Home />} />
          <Route path="products" element={<AddProduct />} />
          <Route path="products/list" element={<AllProduct />} />
          <Route path="products/:id" element={<ProductItem />} />
          <Route path="/edit/:id" element={<UpdateProduct />} />

          <Route path="*" element={<NoPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
