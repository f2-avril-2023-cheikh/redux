import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Outlet, Link } from "react-router-dom";

function Header() {
  return (
    <>
      <Navbar bg="primary" data-bs-theme="dark">
        <Container>
          <Navbar.Brand href="#home">Navbar</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link><Link to="/">Home</Link></Nav.Link>
            <Nav.Link><Link to="/products">Add Products</Link></Nav.Link>
            <Nav.Link><Link to="/products/list">All products</Link></Nav.Link>
          </Nav>
        </Container>
      </Navbar>

      <Outlet />
    </>
  );
}

export default Header;