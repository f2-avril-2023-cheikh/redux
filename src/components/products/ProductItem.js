import React from "react";
import { useSelector } from "react-redux/es/hooks/useSelector";
import { Card, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";

const ProductItem = () => {
  //const params = useParams();

  const product = useSelector((state) => state.product.product);

  return (
    <div>
      {product && (
        <Card style={{ width: "18rem", display: "flex", flexDirection: "row" }}>
          <Card.Img variant="top" src="holder.js/100px180" />
          <Card.Body>
            <Card.Title>{product.name}</Card.Title>
            <Card.Text>
              {product.price} | {product.quantity}
            </Card.Text>
            <Card.Text>{product.description}</Card.Text>
            <Button variant="primary">Details</Button>
          </Card.Body>
        </Card>
      )}
    </div>
  );
};

export default ProductItem;
