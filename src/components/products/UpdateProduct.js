import React from "react";
import { Form, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux/es/hooks/useSelector";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { update } from "../../redux/reducers/productSlice";
import { useNavigate } from "react-router-dom";

const UpdateProduct = () => {
  const { id } = useParams();
  const products = useSelector((state) => state.product.products);
  const productExisting = products.filter((p) => p.id == id);
  const { name, price, quantity, description } = productExisting[0];
  const [product, setProduct] = useState({
    name: name,
    price: price,
    quantity: quantity,
    description: description,
  });
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleUpdate = (e) => {
    e.preventDefault();
    dispatch(
      update({
        id: id,
        name: product.name,
        price: product.price,
        quantity: product.quantity,
        description: product.description,
      })
    );
    navigate("/products/list");
  };

  return (
    <div>
      <div className="container py-3">
        <h3>Modifier Produits</h3>
        <Form>
          <Form.Group className="mb-3" controlId="name">
            <Form.Label>Nom</Form.Label>
            <Form.Control
              name="name"
              type="text"
              placeholder="..."
              value={product.name}
              onChange={(e) => setProduct({ ...product, name: e.target.value })}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="price">
            <Form.Label>Prix</Form.Label>
            <Form.Control
              name="price"
              type="number"
              placeholder="..."
              value={product.price}
              onChange={(e) =>
                setProduct({ ...product, price: e.target.value })
              }
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="quantity">
            <Form.Label>Quantité</Form.Label>
            <Form.Control
              name="quantity"
              type="number"
              placeholder="..."
              value={product.quantity}
              onChange={(e) =>
                setProduct({ ...product, quantity: e.target.value })
              }
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control
              name="description"
              as="textarea"
              rows={3}
              value={product.description}
              onChange={(e) =>
                setProduct({ ...product, description: e.target.value })
              }
            />
          </Form.Group>
          <Button variant="primary" onClick={handleUpdate}>
            Modifier
          </Button>
        </Form>
      </div>
    </div>
  );
};

export default UpdateProduct;
