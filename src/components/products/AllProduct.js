import React from "react";
import Table from "react-bootstrap/Table";
import { Button } from "react-bootstrap";
import { useSelector } from "react-redux/es/hooks/useSelector";
import { useDispatch } from "react-redux";
import { remove, update } from "../../redux/reducers/productSlice";
import { Link } from "react-router-dom";

const AllProduct = () => {
  const products = useSelector((state) => state.product.products);
  const dispatch = useDispatch();

  function removeData(id) {
    dispatch(remove(id));
  }

  return (
    <Table responsive>
      <thead>
        <tr>
          <th>#</th>
          <th>Nom</th>
          <th>Quantité</th>
          <th>Prix</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {products &&
          products.map((value, index) => (
            <tr key={index}>
              <td>{value.id}</td>
              <td>{value.name}</td>
              <td>{value.quantity}</td>
              <td>{value.price}</td>
              <td>{value.description}</td>
              <td>
                <Link
                  to={`/edit/${value.id}`}
                  className="btn btn-primary"
                  variant="primary"
                >
                  Modifier
                </Link>
                <Button variant="danger" onClick={() => removeData(value.id)}>
                  Supprimer
                </Button>
              </td>
            </tr>
          ))}
      </tbody>
    </Table>
  );
};

export default AllProduct;
