import React from 'react';
import {Form, Button} from 'react-bootstrap'
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { add } from '../../redux/reducers/productSlice';

const AddProduct = () => {

  const dispatch = useDispatch();
  const [product, setProduct] = useState({id: '', name: '', price: 0, quantity: 0, description: ''})
  function handleChange(e){
    e.preventDefault();
    setProduct({...product, [e.target.name]: e.target.value})
  }

  function submit(e){
    e.preventDefault();
    dispatch(add({...product, id: Date.now()}));
    console.log(product);
  }

  return (
    <div className="container py-3">
      <Form>
        <Form.Group className="mb-3" controlId="name">
          <Form.Label>Nom</Form.Label>
          <Form.Control name="name" type="text" placeholder="..." onChange={handleChange} />
        </Form.Group>
        <Form.Group className="mb-3" controlId="price">
          <Form.Label>Prix</Form.Label>
          <Form.Control name="price" type="number" placeholder="..." onChange={handleChange}/>
        </Form.Group>
        <Form.Group className="mb-3" controlId="quantity">
          <Form.Label>Quantité</Form.Label>
          <Form.Control name="quantity" type="number" placeholder="..." onChange={handleChange} />
        </Form.Group>
        <Form.Group className="mb-3" controlId="description">
          <Form.Label>Description</Form.Label>
          <Form.Control name="description" as="textarea" rows={3}  onChange={handleChange}/>
        </Form.Group>
        <Button variant="primary" onClick={submit}>Ajouter</Button>
      </Form>
    </div>
  );
}

export default AddProduct;
