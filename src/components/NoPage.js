import React from 'react';

const NoPage = () => {
    return (
        <div>
            Error page not found
        </div>
    );
}

export default NoPage;
