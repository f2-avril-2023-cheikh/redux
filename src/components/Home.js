import React from 'react';
import { useSelector } from 'react-redux/es/hooks/useSelector';
import { Card, Button } from 'react-bootstrap';
import { find } from '../redux/reducers/productSlice';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

const Home = () => {
  const products = useSelector(state => state.product.products).slice(0, 4)
  const dispatch = useDispatch();
  const navigate = useNavigate();
    function view(product){
      dispatch(find(product.id))
      navigate(`/products/${product.id}`)
    }
    return (
      <div>
        {products && products.map((product, index) => (
          <Card key={index} style={{ width: '18rem', display: 'flex', flexDirection: 'row' }}>
          <Card.Img variant="top" src="holder.js/100px180" />
          <Card.Body>
            <Card.Title>{product.name}</Card.Title>
            <Card.Text>
              {product.price} | {product.quantity}
            </Card.Text>
            <Card.Text>
              {product.description}
            </Card.Text>
            <Button variant="primary" onClick={() => view(product)}>Details</Button>
          </Card.Body>
        </Card>
        ))}
      </div>
    );
}

export default Home;
